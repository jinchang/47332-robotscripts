# Test script for Zhenyun
from result_writer import write_results

# Defining variables for test script
incolors = '[red_u,green_u,blue_t,yellow_u]'
cuvette = '1'
vcolors_list = [0.875, 0.875, 0.875, 0.875]
rgb = (80, 56, 21)

# Zhenyun function
write_results(colors=incolors, cuvette=cuvette, vcolors=vcolors_list, rgb=rgb)
