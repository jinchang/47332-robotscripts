# Script for writing results

import os
import datetime
from robot_config import get_data_path


def get_datafile():
    """Return the path of the data file."""
    return os.path.join(get_data_path(), "datafile.csv")


def write_results(colors=None, cuvette=0, vcolors=None, rgb=0):
    """
    Writes results from run_robot script to file on the robot

    colors: list
        list with the input color names as strings
    cuvette: int
        the cuvette mixed into
    vcolors: list
        list with 4 floats. The volumes of each color used
    rgb: tuple
        The rgb code obtained after mixing
    """
    if colors is None:
        colors = []
    if vcolors is None:
        vcolors = []

    datafile = get_datafile()
    if os.path.exists(datafile):
        pass
    else:
        columns = ['Time', 'Vcolors', 'RGB', 'Cuvette', 'Colors']
        datafile_create(columns)

    time = time_getter()

    data = [time, str(vcolors), str(rgb), cuvette, colors]
    with open(datafile, 'a') as f:
        values = ';'.join(data)
        f.writelines('%s\n' % values)
    return


def datafile_create(columns):
    datafile = get_datafile()
    with open(datafile, 'w') as f:
        head = ';'.join(columns)
        f.writelines('%s\n' % head)


def time_getter():
    time0 = datetime.datetime.now()
    return str(time0)[:-7]
