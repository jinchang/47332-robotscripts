# Script for running an AiLEGO_robot run on the robot
import argparse
import time
import json
from result_writer import write_results
from ailego_robot import AiLEGO_robot


# The Setup
parser = argparse.ArgumentParser(
    description='Parsing arguments to the run script!')
parser.add_argument("--cuvette", default='1',
                    help="The next cuvette number to mix in")
parser.add_argument("--ID", default=None,
                    help="The ID number of the run")
parser.add_argument("--vcolors", default='[0.25,0.25,0.25,0.25]',
                    help="The list of color volumes to mix")
parser.add_argument("--xoffset", default='0.0',
                    help="An offset to the x axis positons")
parser.add_argument("--incolors", default='[red_u,green_u,blue_t,yellow_u]',
                    help="The colors mixed")
parser.add_argument("--read_target", action='store_true',
                    help="Should the rgb of '5' also be read?")
parser.add_argument("--pos", default=None,
                    help="What positions should be used")
parser.add_argument("--colorx", default='1.0',
                    help="offset for color reader")

# Load in the arguments
args = parser.parse_args()
cuvette = args.cuvette
x_offset = float(args.xoffset)
color_offset = float(args.colorx)
ID = args.ID
color_list = args.vcolors
color_list0 = color_list.strip('][').split(',')
rough_color_list = [float(n) for n in color_list0]
read_target = args.read_target
incolors = args.incolors
if args.pos:
    pos = json.loads(args.pos)
else:
    pos = args.pos

# Start robot
robot = AiLEGO_robot(ID=ID, x_offset=x_offset, pos=pos)


def normalize_volume(color_list, target_volume=3.5):
    """Normalizes the input color list volumes to some total volume sum.
    Outputs list of same length as input list.
    """
    sum_list = sum(color_list)
    return [target_volume / sum_list * i for i in color_list]


vcolors_list = normalize_volume(rough_color_list)

# Run the robot
robot.run_single_cuvette(cuvette, vcolors_list)

robot.reset_pos()

rgb = robot.read_color(str(cuvette), c_offset=color_offset)
time.sleep(2)

robot.reset_pos()

if read_target:
    rgb5 = robot.read_color('5', c_offset=color_offset)
    time.sleep(2)

robot.reset()

# Resets the robot - syringe - for next run

vcolors_list_norm1 = normalize_volume(vcolors_list, target_volume=1.)

# Write to file on robot here
write_results(colors=incolors, cuvette=cuvette,
              vcolors=vcolors_list_norm1, rgb=rgb)

# Print the output. MUST be last thing in script
if read_target:
    print(rgb, ';', rgb5)
else:
    print(rgb)
