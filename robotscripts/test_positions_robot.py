#!/usr/bin/env python3
# Script for testing positons of containers with the AiLEGO_robot
import argparse
import time
import json

from ailego_robot import AiLEGO_robot

# The Setup
parser = argparse.ArgumentParser(description='Parsing arguments to the run'
                                             'script')
parser.add_argument("--ID", default=None,
                    help="The ID number of the run")
parser.add_argument("--xoffset", default='0.0',
                    help="An offset to the x axis positons")
parser.add_argument("--pos", default=None,
                    help="What positions should be used")
parser.add_argument("--colorx", default='1.0',
                    help="offset for color reader")

# # Load in the arguments
args = parser.parse_args()
ID = args.ID
x_offset = float(args.xoffset)
color_offset = float(args.colorx)


if args.pos:
    pos = json.loads(args.pos)
else:
    # The default value
    pos = args.pos

# Start robot
AiLEGO_robot = AiLEGO_robot(ID=ID, x_offset=x_offset, pos=pos)

# Test that the positions are fine
AiLEGO_robot.test_positions()

AiLEGO_robot.read_color('1', c_offset=color_offset)
time.sleep(2)
AiLEGO_robot.read_color('8', c_offset=color_offset)
time.sleep(2)

# Resets the robot - syringe - for next run
AiLEGO_robot.reset()
