# Script for confirming connection to the robot through AiLEGO_master
import argparse
from ev3dev2.sound import Sound

# Setup
parser = argparse.ArgumentParser(description=('Parsing arguments to the run'
                                              'script'))

parser.add_argument("--say", default='Tell me what to say',
                    help="What the robot should say")


# # Load in the arguments
args = parser.parse_args()
say = args.say

sound = Sound()
sound.speak(say)
