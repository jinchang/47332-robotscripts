import os


def get_path():
    """Get the directory of _this_ file."""
    return os.path.dirname(os.path.abspath(__file__))


def get_data_path():
    base_path = get_path()
    data_path = os.path.join(base_path, "..", "data")
    return os.path.abspath(data_path)
