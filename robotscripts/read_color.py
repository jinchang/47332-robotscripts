# Script for running an AiLEGO_robot run on the robot
import argparse
import time
import json

from ailego_robot import AiLEGO_robot


# The Setup
parser = argparse.ArgumentParser(
    description='Parsing arguments to the run script!')
parser.add_argument("--cuvette", default='1',
                    help="The next cuvette number to mix in")
parser.add_argument("--ID", default=None,
                    help="The ID number of the run")
parser.add_argument("--xoffset", default='0.0',
                    help="An offset to the x axis positons")
parser.add_argument("--pos", default=None,
                    help="What positions should be used")
parser.add_argument("--colorx", default='1.0',
                    help="offset for color reader")

# Load in the arguments
args = parser.parse_args()
cuvette = args.cuvette
x_offset = float(args.xoffset)
color_offset = float(args.colorx)
ID = args.ID
if args.pos is not None:
    pos = json.loads(args.pos)
else:
    pos = None

# Start robot
robot = AiLEGO_robot(ID=ID, x_offset=x_offset, pos=pos)

rgb = robot.read_color(str(cuvette), c_offset=color_offset)
time.sleep(2)

robot.reset_pos()
robot.reset()

# Resets the robot - syringe - for next run
print(rgb)
