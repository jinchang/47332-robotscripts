# Scripts for controlling the LEGO robot

import time
import math
from ev3dev2.sensor import INPUT_1, INPUT_3
from ev3dev2.sensor.lego import TouchSensor, ColorSensor
from ev3dev2.motor import (LargeMotor, SpeedPercent,
                           MediumMotor, OUTPUT_A, OUTPUT_B, OUTPUT_D)


# Notes:
# Removed all logger and the ID maker
# Removed silent keyword and speak
# Removed shake() since it is prone to error
# Removed a lot of color bias work
# Do not used color_diff_squared - comment out
# Keeping some unused functions purely so you COULD run directly on the robot
# KEEP the probably unused averaging in color reader


class AiLEGO_robot(object):
    """
    A class used to control and the 1D LEGO robot

    """

    POSITION_NAMES = ['rinse1', 'red', 'green', '1', '2', '3', '4', '5',
                      '6', '7', '8', '9', '10', 'blue', 'yellow']

    def __init__(self, ID=None, x_offset=0.0, pos=None):
        """

        """
        self.ID = ID
        self.x_offset = x_offset
        self.positions = pos
        self.next_cuvette = 0

        # Initilize positions
        self.ts = TouchSensor(INPUT_3)
        self.cs = ColorSensor(INPUT_1)
        self.z = LargeMotor(OUTPUT_D)
        self.x = MediumMotor(OUTPUT_B)
        self.syr = LargeMotor(OUTPUT_A)

        # operate syringe between 45 (200 muL) and 135(800 muL) degrees
        self.syr_base = 45
        self.syr.on_for_degrees(SpeedPercent(20), self.syr_base)
        # operate syr_pos within 0 and 90 degrees
        self.syr_pos = 0
        # z_pos (vertical position) is set to 0 for fully up, and 180 for fully down
        self.z_pos = 0

        self.reset_pos(rinse=False)
        self.x_pos_max = 10.8

    @property
    def positions(self):
        """Position dictionary for cuvettes, rinse and color tanks."""
        return self._positions

    @positions.setter
    def positions(self, pos):
        if pos is None:
            pos = {'1': 2.9,  # 3.1 before
                   '2': 3.40,
                   '3': 3.9,  # was 4.15 before
                   '4': 4.4,
                   '5': 4.9,
                   '6': 5.4,
                   '7': 5.9,
                   '8': 6.4,
                   '9': 6.9,
                   '10': 7.4,
                   'blue': 8.35,
                   'red': 0.95,
                   'green': 1.9,
                   'yellow': 9.3,
                   'rinse1': 0.,
                   'rinse2': 10.35}
        self._positions = pos

    def set_offset(self, offset):
        self.x_offset = offset

    def x_positions(self, pos):
        """Calculate the positions based on the stored value and offset.

        Args:
            pos (str): name of the cuvette, cartridge or rinse tank
        """
        # position is measured in b engine rotations
        pos = str(pos)

        x_offset = self.x_offset
        if pos == 'rinse1':
            x_offset = 0
        return self.positions[pos] + x_offset

    def move_to_pos_x(self, pos, offset=0):
        """
        Moves to position x based on current position
        and pos in the position dict
        pos is a string with cuvette number or color or rinse1(2)
        """
        xpos = self.x_positions(pos)
        x_move = xpos - self.x_pos + offset
        if xpos + offset > self.x_pos_max:
            raise Exception('Exceeded x_pos_max')
        if xpos + offset < 0:
            raise Exception('Tried to move to negative x')
        self.x.on_for_rotations(SpeedPercent(10), -x_move / 5.)
        self.x.reset()
        self.x_pos = xpos + offset

    def set_syringe(self, degrees):
        """
        Set the syringe position by the degrees of the motor. 

        Args:
            degrees (float): between 0 and 90, which corresponds to 45 and 135 degree range in 
                             on the actual syringe (since the initial value set to 45). 
                             90 means full
                             0 mean empty
        """
        if (degrees > 90. or degrees < 0.):
            raise Exception('Specify degrees in [0, 90] for syringe pump')
        s_move = degrees - self.syr_pos
        self.syr.on_for_degrees(SpeedPercent(40), s_move)
        self.syr_pos = degrees

    def read_color(self, pos, averaging=False, c_offset=1.0):
        """reads the color of cuvette
        Averaged over 5 measurements taken within 1 second if averaging =True

        """
        self.move_to_pos_x(pos, offset=c_offset)
        time.sleep(0.5)
        if averaging:
            color_list = []
            for _ in range(5):
                color_list.append(self.cs.rgb)
                time.sleep(0.2)
            rmean = sum([item[0] for item in color_list]) / len(color_list)
            gmean = sum([item[1] for item in color_list]) / len(color_list)
            bmean = sum([item[2] for item in color_list]) / len(color_list)
            return (rmean, gmean, bmean)

        rgb = self.cs.rgb
        # rgb = tuple([10 * x for x in rgb0])
        # rgb = self.cs.raw
        # print(pos, rgb)
        return rgb

    def move_to_z(self, pos):
        """Move to a given z position
        0 is fully up. 180 is fully down.
        """
        z_move = pos - self.z_pos
        self.z.on_for_degrees(SpeedPercent(20), z_move)
        # self.z.reset()
        self.z_pos = pos

    def pump_syringe_empty(self):
        self.syr.on_for_degrees(SpeedPercent(40), -360)

    def ml_to_degrees(self, vol_ml):
        """
        Converts ml into degrees on the syringe motor
        """
        if vol_ml > 0.6:
            raise Exception('Too large volume. Full pumps should be taken to '
                            + 'reduce below 0.6 before ml_to_degrees function '
                            + 'is used')
        degrees = -180*(-5/4+(1/math.pi) *
                        (math.acos(2*vol_ml/0.85+math.cos(5/4*math.pi))))-90
        return degrees

    def from_color_to_cuvette(self, color, cuvette, vol):
        """
        Moves volume from color cartridge to target cuvette
        Input color and cuvette as string
        vol as float
        """
        completed = False
        while not completed:
            self.move_to_pos_x(color)
            time.sleep(0.5)
            self.move_to_z(180)
            if vol > 0.6:
                self.set_syringe(90)
                vol -= 0.6
            else:
                degrees = self.ml_to_degrees(vol)
                self.set_syringe(degrees)
                completed = True
            time.sleep(1)
            self.move_to_z(0)
            time.sleep(0.5)
            self.move_to_pos_x(cuvette)
            time.sleep(0.5)
            self.move_to_z(45)   # Only move slightly into cuvette.
            time.sleep(1)
            self.set_syringe(0)
            self.pump_syringe_empty()
            self.pump_syringe_empty()
            time.sleep(1)
            self.move_to_z(0)
            time.sleep(1)
            self.reset_pos()

    def fill_cuvette(self, cuvette, color_list):
        """
        cuvette is the next cuvette to be filled: string
        color_list is a list of format :
            [vol_red, vol_green, vol_blue, vol_yellow]
        """
        self.rough_rinse('rinse1')
        color_order = ['red', 'green', 'blue', 'yellow']
        for index, color_vol in enumerate(color_list):
            if color_vol < 0.01:
                continue
            self.from_color_to_cuvette(color_order[index], cuvette, color_vol)
            self.rough_rinse('rinse1')
            self.reset_pos()

    def normalize_volume(self, color_list, target_volume=3.5):
        """Normalizes the input color list volumes to some total volume sum.
        Outputs list of same length as input list.
        """
        sum_list = sum(color_list)
        return [target_volume / sum_list * i for i in color_list]

    def rough_rinse(self, rinse):
        """
        Rinses the syringe by taking the liquid the rinse station and pumping it back once. 
        """
        self.move_to_pos_x(rinse)
        self.move_to_z(180)
        self.pump_syringe_empty()
        self.move_to_z(0)
        # It should already be at that postion but just to make sure
        self.set_syringe(0)
        self.pump_syringe_empty()

    def run_single_cuvette(self, cuvette, rough_color_list):
        """
        cuvette is the number cuvette being mixed in: string
        rough_color_list: list
            unnormalized list of color ratios in format:
                [red, green, blue, yellow]

        """
        cuvette = str(cuvette)
        # The normalization should have been performed already by ailego_master
        # Keeping it since it does not hurt (much)
        color_list = self.normalize_volume(rough_color_list)
        self.fill_cuvette(cuvette, color_list)
        #rgb = self.read_color(cuvette)
        # return rgb
        return

    def run_single_cuvette_auto(self, rough_color_list, **kwargs):
        """
        Similar to run_single_cuvette but gets the cuvette from self
        """
        max_cuvette = 10
        if int(self.next_cuvette) > max_cuvette:
            raise Exception('Ran out of cuvettes')
        self.run_single_cuvette(self.next_cuvette, rough_color_list,
                                **kwargs)
        self.next_cuvette += 1

    def set_first_empty_cuvette(self, empty_cuvette):
        """
        first empty cuvette: int
        """
        self.next_cuvette = empty_cuvette

    def fill_fifth(self, rough_color_list):
        color_list = self.normalize_volume(rough_color_list)
        self.fill_cuvette('5', color_list)

    def reset(self):
        """
        Resets the positons of the z and syring positions
        """
        self.move_to_z(0)
        self.move_to_pos_x('rinse1', offset=0.0)
        self.set_syringe(0)
        self.syr.on_for_degrees(SpeedPercent(20), -self.syr_base)

    def test_positions(self):
        for pos in self.POSITION_NAMES:
            print(pos)
            self.move_to_pos_x(pos)
            time.sleep(0.5)
            self.move_to_z(50)
            time.sleep(0.5)
            self.move_to_z(0)
            time.sleep(0.5)
            # self.reset_pos()

    def color_calibrate(self):
        """ DOES NOT WORK AS INTENDED"""
        self.move_to_pos_x('rinse1')
        self.cs.calibrate_white()
        return

    def reset_pos(self, rinse=True, sleep_time=0.1):
        """Resets the positions using the color sensor
        Args:
            rinse (bool): whether or not to go to the rinsing position first
        """
        if rinse:
            self.move_to_pos_x('rinse1', offset=0.3)

        while True:
            if not self.ts.is_pressed:
                self.x.on_for_rotations(SpeedPercent(10), 0.01)
                time.sleep(sleep_time)
            else:
                break
        self.x_pos = 0

    # def color_dif_squared(self, pos):
    #     """The 'distance' to the target color"""
    #     rgb = self.read_color(pos)
    #     print(rgb)
    #     target_color = self.read_color('5')
    #     print(target_color)
    #     rdif = rgb[0] - target_color[0]
    #     gdif = rgb[1] - target_color[1]
    #     bdif = rgb[2] - target_color[2]
    #     return rdif**2 + gdif**2 + bdif**2
